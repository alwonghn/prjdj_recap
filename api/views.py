

import urllib
from django.shortcuts import render

# def home(request):
#     response = requests.get('https://jsonplaceholder.typicode.com/todos/1')
#     placeholder = response.json()
#     # return render(request,'/templates/home.html',{'userId':placeholder['userId']},{'title':placeholder['title']})
#     return render(request, 'home.html', {'userId':placeholder['userId'],'title':placeholder['title']})
#     # t=get_template('home.html')
#     # html=t.render({'userId':placeholder['userId']}, {'title':placeholder['title']})
#     # return HttpResponse(html)
import json
# import pprint
#
# def home():
#
#     pp=pprint.PrettyPrinter(indent=4)
#
#     request = urllib3.Request('https://dev-corpapp.otago.ac.nz/chargeadmin/addCharge.do');
#
#
# data = {
#     "commandAction": "save",
#     "charge": {"chargeDate": "02082018", "amount": "999.99", "invoiceCreditFlag": "I", "chargeType": {"code": "IDCARD"} },
#     "debitAccount":{"accountCodeOrDefault":"10YGD10773972"},
#     "creditAccount":{"accountCodeOrDefault":"10TVD73143100"}
# }

#
#     request.add_data(json.dumps(data))
#     response = urllib3.urlopen(request)
#     resp_parsed=json.loads(response.read())
#
#     pp.pprint(resp_parsed)


# from rest_framework.test import APIRequestFactory
#
# factory = APIRequestFactory
# request = factory.post('https://dev-corpapp.otago.ac.nz/chargeadmin/addCharge.do',data,content_type='application/json')

from rest_framework.test import APIClient


data = [
    {"commandAction": "save",
    "charge": {
                "chargeDate": "02082018",
                "amount": "999.99",
                "invoiceCreditFlag": "I",
                "chargeType": {"code": "IDCARD"}
                },
    "debitAccount":{"accountCodeOrDefault":"10YGD10773972"},
    "creditAccount":{"accountCodeOrDefault":"10TVD73143100"}
    },
]

#coded = urllib.parse.urlencode(data)
coded = json.dumps(data)

def home(request):
    client = APIClient()
    #response =  client.post('https://dev-corpapp.otago.ac.nz/chargeadmin/addCharge.do',coded, content_type='application/json')
    #response =  client.post('https://jsonplaceholder.typicode.com/todos/1',coded, content_type='application/json')
    response =  client.get('https://jsonplaceholder.typicode.com/todos/1', content_type='application/json')
    #print(response)
    placeholder = response.json()
    #placeholder =  json.loads(response.content)
    # return render(request,'/templates/home.html',{'userId':placeholder['userId']},{'title':placeholder['title']})
    print(response.content_type)
    print(response.status_code)
    #return render(request, 'home.html', {'chargeJustCreated':placeholder['chargeJustCreated'],'id':placeholder['id']})
    return render(request, 'home.html', {'userId':placeholder['userId'],'title':placeholder['title']})
